# Hyper-V VM に CentOS を自動インストールする

## 始める
事前に以下の環境を用意する。

* Hyper-V Host (今回は Windows 10 を使用) - Ansible でリモート管理されるマシン
* Ubuntu (今回は WSL2 を使用) - Ansible を実行するマシン

Ubuntu 側で、利用するパッケージをインストールし、Ansible を動作するようにします。

```
$ sudo apt install python-is-python3 libarchive-tools genisoimage
$ git clone https://gitlab.com/masakura/ansible-hyper-v-sample.git
$ cd ansible-hyper-v-sample
ansible-hyper-v-sample$ python -m venv venv
ansible-hyper-v-sample$ . ./venv/bin/activate
(venv) ansible-hyper-v-sample$ pip install -r requirements.txt
```

`winrm` で Hyper-V Host につながるか確認します。

```
(venv) $ ansible-playbook --vault-id masakura@~/.ansible/default-password -i inventory/masakyra.yml win-ping.yml
```

CentOS の kickstart ISO イメージを作成して、Hyper-V VM を作成しインストールします。

```
(venv) $ ansible-playbook --vault-id masakura@~/.ansible/default-password -i inventory/masakyra.yml install-os.yml
```

およそ 20 分程度かかります。


## 環境の作成
[inventory/masakura.yml](inventory/masakura.yml) を参考に、環境に合わせたインベントリファイルを作成します。

詳しくは [Ansible のインベントリーの構築方法](インベントリーの構築方法
)を参照する。

Hyper-V Host にアクセスするためのパスワードは暗号化しています。

暗号化用のパスワードを作成します。

```
(venv) $ mkdir -p ~/.ansible
(venv) $ pwgen 64 -1 > ~/.ansible/default-password
(venv) $ chmod 600 ~/.ansible/default-passord
```

作成した暗号化用のパスワードを利用してパスワードを暗号化します。

```
$ ansible-vault encrypt_string --vault-id your_inventory@~/.ansible/default-password --name password
Reading plaintext input from stdin. (ctrl-d to end input, twice if your content does not already have a newline)
P@assw0rd
!vault |
          $ANSIBLE_VAULT;1.2;AES256;your_inventory
          31383237376230656137663965633333353632383662393339613162373665376261623336356639
          3532396433623232343832346264343461386237663534370a653337336132666537396365333765
          34613131643034353131313733336562383861313538313161616462336632343761663535316633
          6664363534633938370a383534656235613935643632353164313039373038663137633535383364
          6136
Encryption successful
```

表示された暗号化されたパスワードをインベントリファイルに書き込みます。


## winrm
[Windows リモート管理](https://docs.ansible.com/ansible/2.9_ja/user_guide/windows_winrm.html)を参照。

この Ansible Playbook では、認証に `NTLM`、通陳チャネルに `HTTP (5985)` を利用している。クリーンインストールした Windows Server 2012 R2 や 2016 は標準で利用できる。


## 結果
| Hyper-V Host          | OS                                      | 環境           | 結果
| --------------------- | --------------------------------------- | ------------- | --------------------------------------------------
| win10.example.jp      | Windows 10 Professional Insider Preview | 実機           | OK
| core2012r2.example.jp | Windows Server 2012 R2                  | Hyper-V Guest | Nested Hyper-V がサポートされていないため、VM の起動に失敗
| core2012r2.example.jp | Windows Server 2012 R2                  | Hyper-V Guest | Nested Hyper-V がサポートされていないため、VM の起動に失敗
